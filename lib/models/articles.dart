class Articles {
  String name;
  String genre;
  String price;
  String img;
  String desc;

  Articles(this.name, this.genre, this.price, this.img, this.desc);

  String getName() {
    return this.name;
  }

  String getPrice() {
    return this.price;
  }

  String getGenre() {
    return this.genre;
  }

  String getImg() {
    return this.img;
  }

  String getDesc() {
    return this.desc;
  }
}

List<Articles> armchairs = [
  Articles("Serene Sanctuary", "Chair", "450", "images/1.png", ''),
  Articles("Simba Holding", "Chair", "200",
      "images/Smoke-Armchair-Transparent.png", ''),
];

List<Articles> chairs = [
  Articles("SantaCroz", "Chair", "450", "images/1097341.jpg",
      "Lorem ipsum deri bolded font than"),
  Articles("Mixure chair", "Chair", "200", "images/grace-armchair.jpg",
      "Lorem ipsum deri bolded font than"),
];
