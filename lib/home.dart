import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'models/articles.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int selected = 0;
  List<Map> genre = [
    {
      "nom": "Armchair",
      "icon": Icons.change_history_sharp,
    },
    {
      "nom": "Bed",
      "icon": Icons.king_bed_rounded,
    },
    {
      "nom": "Cupboard",
      "icon": Icons.countertops_sharp,
    },
    {
      "nom": "Tapis",
      "icon": Icons.table_chart,
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
            color: Colors.grey.shade500,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12), topRight: Radius.circular(12))),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        height: 60,
        //color: Colors.red,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            BottomNavItem(
              txt: "Home",
              activated: true,
              icon: Icons.home,
              press: () {},
            ),
            BottomNavItem(
              txt: "Likes",
              icon: Icons.king_bed,
              press: () {},
              activated: false,
            ),
            BottomNavItem(
              txt: "Notifs",
              icon: Icons.notification_important,
              press: () {},
            ),
            BottomNavItem(
              txt: "Basket",
              icon: Icons.shopping_basket,
              press: () {},
            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child:
                CircleAvatar(backgroundImage: AssetImage("images/perso.png")),
          )
        ],
        backgroundColor: Colors.white,
        elevation: 0,
        leading: Icon(
          Icons.clear_all_rounded,
          color: Colors.black,
        ),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  //height: 100,
                  //decoration: BoxDecoration(color: Colors.blue),
                  child: Text(
                    "Best forniture\nfor your home",
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12),
                          child: TextField(
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Search',
                                icon: Icon(
                                  Icons.search,
                                  color: Colors.grey.shade700,
                                )),
                          ),
                        ),
                        height: 50,
                        width: MediaQuery.of(context).size.width * 0.8,
                        //color: Colors.red,
                        decoration: BoxDecoration(
                            color: Colors.grey.shade300,
                            borderRadius: BorderRadius.circular(10)),
                      ),
                      Container(
                          child: IconButton(
                              icon: Icon(
                                Icons.rotate_90_degrees_ccw,
                                color: Colors.grey,
                              ),
                              onPressed: () {
                                print("pressed");
                              }),
                          height: 50,
                          width: MediaQuery.of(context).size.width * 0.1,
                          //color: Colors.red,
                          decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(10))),
                    ],
                  ),
                ),
                Container(
                  height: 60,
                  //color: Colors.red,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: genre.length,
                    itemBuilder: (context, index) => Padding(
                      padding:
                          const EdgeInsets.only(right: 12, top: 12, bottom: 12),
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            selected = index;
                          });
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                color: selected == index
                                    ? Colors.black
                                    : Colors.grey.shade300,
                                borderRadius: BorderRadius.circular(12)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Icon(
                                    genre[index]['icon'],
                                    color: selected == index
                                        ? Colors.white
                                        : Colors.grey.shade700,
                                    size: 20,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    genre[index]['nom'],
                                    style: TextStyle(
                                        color: index == selected
                                            ? Colors.white
                                            : Colors.grey.shade700),
                                  ),
                                ],
                              ),
                            )),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: Container(
              height: 30,
              //color: Colors.red,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Armchair",
                    style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
                  ),
                  GestureDetector(
                      onTap: () {
                        setState(() {
                          print('pressed');
                        });
                      },
                      child: Text('View all'))
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Container(
              alignment: Alignment.center,
              height: 270,
              //color: Colors.red,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: armchairs.length,
                  itemBuilder: (context, index) => Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          //height: 250,
                          width: MediaQuery.of(context).size.width * 0.43,
                          //color: Colors.blue,
                          child: Stack(
                            children: [
                              Positioned(
                                  top: 0,
                                  child: Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.43,
                                    height: 200,
                                    //color: Colors.yellow,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            //fit: BoxFit.fill,
                                            image: AssetImage(
                                                armchairs[index].getImg())),
                                        color: Colors.grey.shade400,
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                  )),
                              Positioned(
                                  bottom: 5,
                                  right: 10,
                                  left: 10,
                                  child: Column(
                                    children: [
                                      Text(
                                        armchairs[index].name,
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            armchairs[index].getGenre(),
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            armchairs[index].getPrice() + "\$",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          )
                                        ],
                                      ),
                                    ],
                                  ))
                            ],
                          ),
                        ),
                      )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: Container(
              height: 30,
              //color: Colors.red,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Latest Products",
                    style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
                  ),
                  GestureDetector(
                      onTap: () {
                        setState(() {
                          print('pressed');
                        });
                      },
                      child: Text('View all'))
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Container(
              height: 70,
              //color: Colors.blue,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: chairs.length,
                itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    //color: Colors.white,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.2),
                            spreadRadius: 1,
                            blurRadius: 0,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ],
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.white),
                    //height: 30,
                    child: Row(
                      children: [
                        Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              //color: Colors.red,
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: AssetImage(
                                    chairs[index].getImg(),
                                  ))),
                        ),
                        SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              chairs[index].getName(),
                            ),
                            Text(chairs[index].getGenre()),
                            Text(
                              chairs[index].getDesc(),
                              style: TextStyle(
                                  fontSize: 11, fontWeight: FontWeight.w300),
                            ),
                            //Text('tested'),
                          ],
                        ),
                        SizedBox(width: 20),
                        Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Text(chairs[index].getPrice() + "\$"),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class BottomNavItem extends StatelessWidget {
  final String txt;
  final IconData icon;
  final Function press;
  final bool activated;
  const BottomNavItem({
    Key key,
    this.activated = false,
    this.txt,
    this.icon,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Icon(icon, color: activated ? Colors.black : Colors.white),
          Text(
            txt,
            style: TextStyle(color: activated ? Colors.black : Colors.white),
          )
        ],
      ),
    );
  }
}
